# Setup when dev
After cloning, run
- npm i
- npx prisma pull (if db schema changed)
- npx prisma generate
- npm start