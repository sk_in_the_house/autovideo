const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");

// Use stealth plugin
puppeteer.use(StealthPlugin());

let browser;
let page;
let headless = true;
// let executablePath;

async function init() {
    // We need to disable sandbox, otherwhise doesn't run on digital ocean vm
    // We need this screen size, so that when writing the creator comment, we have the desktop view and not a tablet optimised view
    let config = {
        headless: headless,
        userDataDir: __dirname + '/chromeData',
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--window-size=1280,900'
        ],
        // executablePath: '/snap/bin/chromium'
    };

    // Change chrome executable path when necessary
    // if(executablePath){
    //     config["executablePath"] = executablePath;
    // }

    // Launch browser
    browser = await puppeteer.launch(config);

    page = await browser.newPage();
}

// function setExecutable(path){
//     executablePath = path;
// }

function getPage() {
    return page;
}

async function newPage() {
    let page = await browser.newPage();
    return page;
}

async function close() {
    await browser?.close();
}

function toggleHeadless(bool) {
    headless = bool;
}

async function goto(url:string) {
    page.goto(url);
}

async function resetBrowser() {
    await init();
    const client = await page.target().createCDPSession();
    await client.send('Network.clearBrowserCookies');
    await client.send('Network.clearBrowserCache');
    await browser.close();
}

export {
    init,
    getPage,
    close,
    toggleHeadless,
    goto,
    resetBrowser,
    newPage,
    // setExecutable
}