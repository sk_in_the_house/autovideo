interface uploadSpecs {
    channelId: string;
    video: string;
    pageOne: {
        title: string;
        description?: string;
        thumb?: string;
        playlists?: string[];
        madeForKids?: boolean;
        restrictForAdultsOnly?: boolean;
        paidPromotion?: boolean;
        allowAutomaticChapters?: boolean;
        tags?: string[];

    };
    pageTwo?: {
        endcard?:{
            enabled: boolean;
            import: boolean;
            importId?: string;
            type?: endcard;             

        };
        cards?: {
            enabled: boolean;
            type: card;
            name: string;
            message?: string;
            teaser?: string;
            anyVideo?: boolean;
        }
    };
    pageThree?: {

    };
    pageFour: {
        visibility: visibility;
        instantPremiere?: boolean;
    };
    creatorComment?:{
        text:string;
        pin?: boolean;
    }

}

enum endcard {
    T1 = 1,
    T2 = 2,
    T3 = 3,
    T4 = 4,
    T5 = 5,
    T6 = 6,
}

enum card {
    VIDEO = 2,
    PLAYLIST = 3,
    CHANNEL = 4,
    LINK = 5,
}

enum visibility {
    PRIVATE = "PRIVATE",
    UNLISTED = "UNLISTED",
    PUBLIC = "PUBLIC",
}

export {
    uploadSpecs,
    card,
    endcard,
    visibility
}


