import * as yt from "./apilessyt";
import dotenv from "dotenv";
import path from "path";

dotenv.config({ path: path.join(`${__dirname}/../.env`) });

export async function login() {

  try {
    yt.setHead(false);
    console.log("Logging in...");

    await yt.reset();
    await yt.login({
      name: process.env.YT_MAIL,
      password: process.env.YT_PASSWORD,
      twofa: Boolean(process.env.YT_TFA),
    });
    console.log("Done!");
  } catch (e) {
    console.log(e)
  }

}

export async function reset() {

  try {
    yt.setHead(false);
    console.log("Resetting...");

    await yt.reset();
    console.log("Done!");
  } catch (e) {
    console.log(e)
  }

}
