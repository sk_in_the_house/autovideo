import express from 'express';
import cors from 'cors';
import { createVideo } from './main';
import { login, reset } from './googleLogin';

const app = express();
const PORT = 3021;

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/compiler/uploadVideo/:duration', async (req, res) => {
    let duration = Number(req.params.duration) ?? 1;
    createVideo(duration);
    res.sendStatus(200);
});

app.get('/compiler/login', async (req, res) => {
    login();
    res.sendStatus(200);
});

app.get('/compiler/reset', async (req, res) => {
    reset();
    res.sendStatus(200);
});

app.get("/compiler", (req, res) => {
    res.status(200).send("Compiler online ✅");
});

app.listen(PORT, () => console.log(`AutoCompiler running on port ${PORT}`));