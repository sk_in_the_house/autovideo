interface footage {
  id: number;
  url: string;
  title: string;
  timestamp: Date;
  size: number;
  duration: number;
  used: boolean;
}

enum services {
  DISCORD_SERVICE = "discordService",
  REDDIT_SERVICE = "redditService",
  TIKTOK_SERVICE = "tiktokService"
}

const servicePorts = {
  "discordService": 3001,
  "redditService": 3002,
  "tiktokService": 3003
}

export { footage, services, servicePorts };
