import * as yt from "./apilessyt";
import path from "path";

let videoDir = path.join(`${__dirname}/../videos`);
let duration = 1;

(async () => {
    await yt.setHead(true)
    await yt.upload({
        channelId: "aUoFJoPY0s48SeikepdCghwA",
        video: `${videoDir}/Meme status.mp4`,
        pageOne: {
            // Title can be max 100 chars long!
            title: 'Meme Status',
            description:
                "💖 Thank you for watching 💖\n\nIf you enjoy what you are seeing feel free to subscribe and hit the bell 🔔!\nWrite down in the comments what you think about the Video👉👈!\n\nIf you want a clip of yours removed or want to contact me for any other reason don't hesitate to send me an email.\n👇👇👇👇\nautovideo@gmail.com\n👆👆👆👆\n\nwhy awe you stiww weading the descwiption owo? what awe you doing down hewe uwu? if you have wead this faw pwease shawe this video with youw fwiends🥺👉👈\n\ntags:\n#memes #videos #tiktiok\n\nmemes, meme, friday night funkin memes, fnf memes, bernie sanders memes, grubhub memes, perfectly cut memes, dankest, tiktok Memes, minecraft memes, cute animals memes, spongebob Memes, dank memes, try not to laugh, dank compilation, memes, funny memes, pewdiepie, tiktok, big chungus, freememeskids, unususal videos, meme vine, dank meme compilation, best memes, rip vine, unusual videos compilation, fortnite memes, dank memes compilation, grandayy, pewdiepie vs tseries, airpods, dank, fresh memes, filthy frank, hefty, roblox memes, yoda memes, gta memes, youtube rewind memes, weird memes, belle delphine, rick rolled, memecorp, lego city memes, best memes compilation, joker memes, callmecarson, wide putin, funny cat memes, Ugh, fine, I guess you are my little Pogchamp, spiderman memes, shaq memes,, boom beach memes, monke memes, mario memes, cj memes, breaking bad walter memes, cyberbug 2077 memes, bingus memes, wake up omegle memes, jojo's bizzare adventure memes, gordon ramsay memes, tyler1 memes, spiderman memes, aunt cass memes, mr bean memes, dababy memes,  rick and morty memes, patrick memes, amogus memes, dog memes, emisoccer memes, deadpool memes, aunt cass memes, friday night funkin memes, dababy memes, mr bean memes, mario memes, peter griffin memes, south park memes, no god pls no meme, quagmire memes, twomad memes, edp445 memes, trade offer memes, don't flirt with him memes, lightning mcqueen, joe biden memes, dank doodle memes, dean norris memes",
            thumb: `${videoDir}/tempThumbnail.png`,
        },
        pageTwo: {
            cards: {
                enabled: true,
                type: yt.types.card.PLAYLIST,
                name: "PLB-Sd_X8jxPyLcnk6-3WshvS10XZpT7jJ",
                anyVideo: true,
            },
            endcard: {
                enabled: duration > 1 ? true : false,
                import: true,
                importId: 'ygwfav9jUEP'
            }
        },
        pageFour: {
            visibility: duration > 1 ? yt.types.visibility.PRIVATE : yt.types.visibility.PUBLIC,
        },
        creatorComment: {
            text: "join discord for source: https://discord.gg/kAFf4agIvK\nalso don't forget to like the video :]",
            pin: true,
        },
    });
})();
