import axios from "axios";
import notifyUs from "./notifier/index.js";
import express from "express";
import dotenv from "dotenv";
import _ from "lodash";
import getVideoDurationInSeconds from 'get-video-duration';
import path from "path";
import { footage, target } from "./interface/types";
import spawn from "await-spawn";
import { PrismaClient } from '@prisma/client'

dotenv.config({ path: path.join(`${__dirname}/../.env`) });

const prisma = new PrismaClient();
const PORT = 3001;
const privateDiscordSource: target = {
  target_server: "863389934195179541",
  target_channel: "873937287006810152"
}

// Cache
let cache = [];
let cacheTarget = 8 * 60;
let cacheRestockTrigger = 4 * 60;
let restocking = false;

// General
let scrapingTargets = [];
let emittedFootage = []; // Store posts that have been requested, but not yet used, as to avoid sending duplicates if multiple clients request videos

async function getFootage(minimumDuration: number, targets, oldFootage: footage[]): Promise<footage[]> {

  // Loop over channels till we have enough footage
  let footage: footage[] = [];
  let footageDuration: number = 0;

  targets = _.shuffle(targets);

  for (let i = 0; i < targets.length; i++) {

    const { scrapedFootage, scrapedFootageDuration } = await scrapeChannel(targets[i], minimumDuration, footageDuration, [...footage, ...oldFootage]);

    // Repeat current loop if we hit a rate limit
    if (scrapedFootageDuration == -1) {
      i -= 1; continue;
    }

    footageDuration += scrapedFootageDuration;
    footage = [...footage, ...scrapedFootage];

    // Check if we have all the footage we need
    if (footageDuration >= minimumDuration) {
      console.log("Got the requested footage of " + minimumDuration + "s!");
      break;
    }

  }

  // Check if we ran out of channels before satisfying the minimumDuration requirement
  if (footageDuration < minimumDuration) {
    console.log("Didn't find enough footage, please add more channels for scraping in the future!");
    notifyUs("DiscordService - Didn't find enough footage, please add more channels for scraping!");
  }

  return footage;
}


/**
 * @param {Object.<Target>} target - An object containing the (server) and (channel) to scrape
 * @param {Number} minimumDuration - The footage duration we want to reach
 * @param {Number} scrapedDuration - The current footage duration that has been scraped prior to this function call
 * @param {Array.<Footage>} blacklistedFootage - Footage that has been scraped before (prev. sessions and current session)
 * @returns {Object} - The object contains an array of footage (scrapedFootage) and the duration of the scraped footage (scrapedFootageDuration)
 */
async function scrapeChannel(target: target, minimumDuration: number, scrapedDuration: number, blacklistedFootage: footage[]): Promise<{ scrapedFootage: footage[], scrapedFootageDuration: number }> {

  // Variables
  let scrapedFootage: footage[] = [];
  let offset: number = 0;
  let lastMessage: any = null;
  console.log(`Scraping channel ${target.target_channel} on server ${target.target_server}...`);

  // Main loop
  while (scrapedDuration < minimumDuration) {
    try {

      // Scrape messages via the search function
      console.log(`Scraping at offset ${offset}...`);

      // If we are scraping our own private source channel, then don't filter for videos, as we also allow reddit-, tiktok-, instagram- and twitter-links
      let hasVideo = "has=video&";
      if (JSON.stringify(target) === JSON.stringify(privateDiscordSource)) {
        hasVideo = "";
      }

      // Run request
      let res;
      try {
        res = await axios.get(`https://discord.com/api/v9/guilds/${target.target_server}/messages/search?${hasVideo}channel_id=${target.target_channel}&offset=${offset}`, {
          headers: {
            Authorization: process.env.DISCORD_USER_TOKEN,
            "Content-Type": "application/json",
          },
        });
      } catch (error) {

        if (error.response?.status == 429) {
          // Handle all kinds (shared, global, user) rate limiting (see https://discord.com/developers/docs/topics/rate-limits for reference)

          let rateLimitTime = parseInt(error.response?.headers["retry-after"]) + 1 || 10;
          let rateLimitType = error.response?.headers["x-ratelimit-scope"];

          console.log("Hit a " + rateLimitType + " rate limit. Waiting for " + rateLimitTime + "s...");
          await new Promise(resolve => setTimeout(resolve, rateLimitTime * 1000));
          console.log("Trying again...");

          return { scrapedFootage: [], scrapedFootageDuration: -1 }; // repeats the current request again

        } else {
          // Handle other erros
          console.log("Error while requesting! Maybe discord is down? Returning an empty array for now...");
          return { scrapedFootage: [], scrapedFootageDuration: 10000 }; // returning huge duration so that disordService returns an empty array (this case is handled by filterserviceServer)
        }

      }

      // End of page check
      if (lastMessage === res.data.messages[res.data.messages.length - 1]) {
        console.log("Reached end of messages for this channel, moving on...");
        break;
      }

      // Filter for attached videos and save them as footage objects
      // Res structure: messages -> [ { ..., attachments: [...] }, ... ] -> attachement -> { url, ... }
      for (let i = 0; i < res.data.messages.length; i++) {
        const msgContainer = res.data.messages[i];
        for (let l = 0; l < msgContainer.length; l++) {
          const message = msgContainer[l];

          /**
           *  Social data media structures:
           *  yt -> .embed -> url/title
           *  reddit -> .embed -> url/title
           *  twitter -> embed -> url/description
           *  raw video -> .attachment -> url/title
           *  insta -> .content (pure link)
           *  tiktok -> .content (pure link)
           **/

          // Handle attachments
          for (let j = 0; j < message.attachments.length; j++) {
            const attachment = message.attachments[j];

            // Check duration because there can be multiple attachments
            if (scrapedDuration >= minimumDuration) {
              break;
            }

            // Filter out attached images
            if (attachment.content_type.includes("image") || attachment.url.includes("gif")) {
              continue;
            }

            // Get duration
            let duration = await getVideoDurationInSeconds(attachment.url);

            // Check if we have already used the video in an old session
            if (blacklistedFootage.some((footage) => footage.url === attachment.url || (footage.size === attachment.size && footage.duration === duration))) {
              continue;
            }

            // Check if we have already used the video in the current session
            if (scrapedFootage.some((footage) => footage.url === attachment.url || (footage.size === attachment.size && footage.duration === duration))) {
              continue;
            }

            if (duration < 30 && attachment.url) {
              // Save duration
              scrapedDuration += Math.floor(duration);
              // Push footage object
              scrapedFootage.push({
                url: attachment.url,
                title: attachment.filename.split(".")[0],
                timestamp: new Date(),
                size: attachment.size,
                duration: Math.floor(duration),
                used: false
              });
            }
          }

          // Our own private discord channel where we post links to content we like (only this channel is whitelisted for embeds and other links)
          // NOTE ON DUPLICATE CHECKS: We do not store the duration or size for embeds and normal links, as the size property is not really obtainable
          // So we just set size and duration for the objects to 0 which is fine, since we won't post duplicate content on our own private channel
          if (JSON.stringify(target) === JSON.stringify(privateDiscordSource)) {
            // Handle embeds (only for our own private channel on our server, so that we can scrape yt videos)
            for (let k = 0; k < message.embeds.length; k++) {
              const embed = message.embeds[k];

              // Check duration because there can be multiple embeds
              if (scrapedDuration >= minimumDuration) {
                break;
              }

              // Filter out embeded images (specifically twitter)
              if (embed.url.includes("photo") || embed.url.includes("gif")) {
                continue;
              }

              // Get duration
              let res = await spawn("youtube-dl", [embed.url, "--get-duration"]);
              let duration = res.toString();

              // Check if we have already used the video in an old session
              if (blacklistedFootage.some((footage) => footage.url === embed.url)) {
                continue;
              }

              // Check if we have already used the video in the current session
              if (scrapedFootage.some((footage) => footage.url === embed.url)) {
                continue;
              }

              if (duration < 30 && embed.url) {
                // Save duration
                scrapedDuration += Math.floor(duration);
                // Push footage object
                scrapedFootage.push({
                  url: embed.url,
                  title: embed.title || embed.description || "",
                  timestamp: new Date(),
                  size: 0,
                  duration: 0,
                  used: false
                });

              }

            }

            // Handle normal links (insta and tiktok only)
            if (message.content.includes("https://www.tiktok.com") || message.content.includes("https://www.instagram.com")) {
              // Extract link from message text
              let urlRegex = /(https?:\/\/[^ ]*)/;
              let url = message.content.match(urlRegex)[1];

              // Check if we have already used the video (in the current or a previous session)
              if (blacklistedFootage.some((footage) => footage.url === url)) {
                continue;
              }
              if (scrapedFootage.some((footage) => footage.url === url)) {
                continue;
              }

              // Get duration
              let res = await spawn("youtube-dl", [url, "--get-duration"]);
              let duration = res.toString();

              if (duration < 30) {
                // Save duration
                scrapedDuration += Math.floor(duration);
                // Push footage object
                scrapedFootage.push({
                  url: url,
                  title: message.id,
                  timestamp: new Date(),
                  size: 0,
                  duration: 0,
                  used: false
                });
              }

            }
          }
        }

        // Exit if we have enough footage inside of the loop
        if (scrapedDuration >= minimumDuration) {
          break;
        }

      }

      // Set last messages for end of chat check
      lastMessage = res.data.messages[res.data.messages.length - 1];

      // Increase offset for pagination
      offset += 25;

    } catch (error) {
      console.error("There was an error querying discord messages! Is the server/channel id correct?", error);
    }
  }

  return { scrapedFootage, scrapedFootageDuration: scrapedDuration };
}

// Database

async function loadServiceData(): Promise<{ targets: target[], footage: footage[] }> {
  const footage = await prisma.footage.findMany() || [];
  const targets = await prisma.config.findMany();
  // let targets: target[] = [{
  //   server: "867887039117262848",
  //   channel: "867887039117262851"
  // }];
  console.log('Successfully loaded used footage!');
  // @ts-ignore: Footage duration cast from Decimal to Number even tough DB uses Int...
  return { targets, footage };
}

async function updateServiceData(footage: footage[]): Promise<void> {
  if (footage.length) {
    await prisma.footage.createMany({ data: footage }).catch(e => console.log(e));
    console.log("Inserted into database!");
  }
}

function totalDuration(footage: footage[]): number {
  let duration = 0;
  footage.forEach(footage => {
    duration += footage.duration
  });
  return duration;
}

function getCachedFootage(minimumDuration: number): footage[] {

  let duration = 0;
  let footageArr = [];

  for (let i = 0; i < cache.length; i++) {

    const footage = cache[i];
    footageArr.push(footage);
    duration += footage.duration;

    if (duration >= minimumDuration) {
      break;
    }
  }

  // Remove footageArr from cache
  cache = cache.filter(footage => !footageArr.includes(footage));

  return footageArr;
}

// Express

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/discordService", (req, res) => {
  res.status(200).json({
    status: "DiscordService online ✅",
    cacheDuration: totalDuration(cache),
    cache,
    emittedDuration: totalDuration(emittedFootage),
    emittedFootage
  });
});

app.get("/discordService/getFootage/:minimumDuration?", async (req, res) => {

  // Footage section
  let minimumDuration: number = Number(req.params.minimumDuration) ?? 1;
  console.log("---------------------------");
  console.log(`Received footage request at ${new Date().toLocaleString("en-GB")} of ${minimumDuration}s...`);

  let footage = getCachedFootage(minimumDuration);
  emittedFootage = [...emittedFootage, ...footage];
  res.status(200).json(footage);

  // If cache is below restockTrigger, restock cache up to cacheTarget
  let cacheDuration = totalDuration(cache);

  if (cacheDuration < cacheRestockTrigger && !restocking) {
    restocking = true;

    let durationNeeded = cacheTarget - cacheDuration;
    console.log("Restocking cache (" + durationNeeded + "s)...");
    let additionalFootage: footage[] = await getFootage(durationNeeded, scrapingTargets, emittedFootage);

    cache = [...cache, ...additionalFootage];
    restocking = false;
  }

});

app.post("/discordService/setFootageUsed", async (req, res) => {
  let footage: footage[] = req.body.footage;
  console.log('Trying to insert: ', footage)
  updateServiceData(footage);
  res.status(200).json("Updated footage!");
});

(async () => {

  // Targets
  let { targets, footage } = await loadServiceData();
  scrapingTargets = targets;
  emittedFootage = footage;

  // Cache 6 mins of footage
  console.log("Caching " + cacheTarget / 60 + " minutes of footage...");
  cache = await getFootage(cacheTarget, scrapingTargets, emittedFootage);

  // Server
  app.listen(PORT, () => console.log(`DiscordService running on port ${PORT}`));

})();


// TESTING CODE, DO NOT DELETE!
// (async () => {
//   let targets: target[] = [
//     { target_server: '762115688881586186', target_channel: '762458221641596938' },
//     { target_server: '270613445177638922', target_channel: '874447411144171610' },
//     { target_server: '359801125882298378', target_channel: '360103749844336640' },
//     { target_server: '863389934195179541', target_channel: '863695602351996939' }
//   ]
//   let res = await scrapeChannel(_.sample(targets), 30, 0, []);
//   console.log(res)
// })();
