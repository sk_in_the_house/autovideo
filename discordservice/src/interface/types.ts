interface footage {
  id?: number;
  url: string;
  title: string;
  timestamp: Date;
  size: number;
  duration: number;
  used: boolean;
}

interface target {
  target_server: string;
  target_channel: string;
}

export { footage, target };
