import axios from 'axios';

export default async function notifyUs(message: string) {
    await axios.get('https://maker.ifttt.com/trigger/notify_us_on_bot_upload/with/key/dfI9OfM5roZ_0s0GuF_euv?value1=' + message).catch(error => { console.error("Couldn't send push notifications to us!", error) });
    console.info("Sent notification!");
}