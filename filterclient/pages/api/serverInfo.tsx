import publicIp from 'public-ip';

export default async function handler(req, res) {
  res.status(200).json({ ip: await publicIp.v4() });
}