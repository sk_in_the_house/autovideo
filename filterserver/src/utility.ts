import axios from 'axios';
import { footage, servicePorts, services, stream } from './types';
import spawn from 'await-spawn';
import path from 'path';
import dateFormat from "dateformat";
import logger from "node-color-log";
import _ from 'lodash';

const { DISCORD_SERVICE, REDDIT_SERVICE } = services;

export async function requestVideo(clientId): Promise<stream> {
    console.log("waiting for video...")
    // Get footage
    let duration = 1;
    let [discordLinks, redditLinks] = await requestServiceFootage(duration);
    let links: footage[] = [...discordLinks, ...redditLinks]
    let url = links[0].url;
    let service = discordLinks.length ? DISCORD_SERVICE : REDDIT_SERVICE;

    // Download and send video name + url
    let videoName = url.replace(/[^a-z0-9]/gi, '_').toLowerCase() + ".mp4";
    try {
        console.log(url);
        await spawn("youtube-dl", [url, "-o", path.join(__dirname + `/../videos/${clientId}/${videoName}`)]);
        console.log(`Downloaded video!`);
    } catch (err) {
        console.log(`Error downloading video!`);
        console.log(err);
        return null;
    }

    return { videoName, footage: links[0], service };
}


async function requestServiceFootage(duration: number): Promise<footage[][]> {
    let serviceResponses = [];

    // Single video
    if (duration == 1) {
        let min = 1;
        let max = 3;
        let randNumb = Math.floor(Math.random() * (max - min + 1) + min); // min max included

        switch (randNumb) {
            case 1:
                serviceResponses = [await getFootage(DISCORD_SERVICE, 1), []];
                break;
            case 2:
                serviceResponses = [[], await getFootage(REDDIT_SERVICE, 1)];
                break;
            case 3:
                serviceResponses = [await getFootage(DISCORD_SERVICE, 1), []];
                break;
        }
    } else {
        // Compilation
        serviceResponses = await Promise.all([
            getFootage(DISCORD_SERVICE, Math.floor(duration * 0.5) || 1),
            getFootage(REDDIT_SERVICE, Math.floor(duration * 0.5) || 1),
            // getFootage(TIKTOK_SERVICE, Math.floor(duration * 0.2)),
        ]);
    }

    return serviceResponses;
}

async function getFootage(service: services, minimumDuration: number): Promise<footage[]> {
    try {
        log(`Requesting ${minimumDuration}s of footage from ${service}. Waiting for server response...`);
        let res = await axios.get(`http://${service.toLowerCase()}:${servicePorts[service]}/${service}/getFootage/${minimumDuration}`);
        return res.data;
    } catch (err) {
        error(`Request to ${service} failed!`, err);
    }
}

export async function setUsedFootage(service: services, links: footage[]): Promise<void> {
    try {
        if (links?.length > 0) {
            log(`Telling ${service} what footage we used...`);
            await axios.post(`http://${service.toLowerCase()}:${servicePorts[service]}/${service}/setFootageUsed`, { footage: links });
        }
    } catch (err) {
        error(`Telling ${service} what footage we used...`, err);
    }
}

export async function insertValidatedFootage(footage: footage): Promise<void> {
    try {
        if (footage) {
            log(`Telling sending filterservice the validated footage..`);
            await axios.post(`http://footageservice:3004/footageService/addFootage`, { footage });
        }
    } catch (err) {
        error(`Telling sending filterservice the validated footage..`, err);
    }
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function log(msg: string, date: boolean = false): void {
    let e = new Error();
    let frame = e.stack.split("\n")[2];
    let functionName = frame.split(" ")[5];
    logger.log(`[${functionName}] ${date ? dateFormat(new Date(), "yyyy-mm-dd@hh:MM:ss") + " " : ""}`).joint().bold().log(msg);
}

function error(msg: string, error: string): void {
    let e = new Error();
    let frame = e.stack.split("\n")[2];
    let functionName = frame.split(" ")[5];
    logger.bold().bgColor("red").log("[ERROR]").joint().log(`[${functionName}] ${dateFormat(new Date(), "yyyy-mm-dd@hh:MM:ss")} `).joint().bold().log(" " + msg);
    logger.log(error);
}
