-- POSTGRES BULLSHIT:
-- Dont use camelcase, not supported in postgres (except with quotation marks)
-- Double quotes for identifiers (column names, etc.)
-- Single quotes for string values

-- Reddit Service

CREATE DATABASE redditservice;
\connect redditservice

CREATE TABLE footage (
  id SERIAL PRIMARY KEY NOT NULL,
  url VARCHAR(255) UNIQUE NOT NULL,
  title VARCHAR(255),
  timestamp TIMESTAMP,
  duration INTEGER,
  size INTEGER,
  used BOOLEAN NOT NULL DEFAULT false
);

-- Discord Service

CREATE DATABASE discordservice;
\connect discordservice

CREATE TABLE footage (
  id SERIAL PRIMARY KEY NOT NULL,
  url VARCHAR(255) UNIQUE NOT NULL,
  title VARCHAR(255),
  timestamp TIMESTAMP,
  duration INTEGER,
  size INTEGER,
  used BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE config (
  target_server VARCHAR(255) NOT NULL,
  target_channel VARCHAR(255) NOT NULL,
  PRIMARY KEY(target_server, target_channel)
);

INSERT INTO config (target_server, target_channel)
VALUES
  ('560619596927991818', '560640659669778452'),
  ('768263169432223745', '768316715934285844'),
  ('768263169432223745', '773668004004757514'),
  ('226368006115033091', '546763235051700314'),
  ('542571000902975508', '542572302194442240'),
  ('398590278404931584', '717498714259980378'),
  ('495299641542180874', '820838529525088286'),
  ('762115688881586186', '762458221641596938'),
  ('270613445177638922', '874447411144171610'),
  ('359801125882298378', '360103749844336640'),
  ('863389934195179541', '863695602351996939');

-- TikTOk Service
  -- tbd

-- Thumbnail Service

CREATE DATABASE thumbnailservice2;
\connect thumbnailservice2

CREATE TABLE thumbnail (
  id SERIAL PRIMARY KEY NOT NULL,
  sourceurl VARCHAR(255) UNIQUE NOT NULL,
  "file" TEXT NOT NULL,
  used BOOLEAN NOT NULL DEFAULT false
);

-- Footage Service

CREATE DATABASE footageservice;
\connect footageservice

CREATE TABLE validatedfootage (
  id SERIAL PRIMARY KEY NOT NULL,
  url VARCHAR(255) UNIQUE NOT NULL,
  title VARCHAR(255),
  timestamp TIMESTAMP,
  duration INTEGER,
  size INTEGER,
  used BOOLEAN NOT NULL DEFAULT false
);

-- AutoCompiler

CREATE DATABASE autocompiler;
\connect autocompiler

CREATE TABLE compilation (
  id SERIAL PRIMARY KEY NOT NULL,
  version_number INTEGER NOT NULL
);

INSERT INTO compilation (version_number) VALUES (1);

-- Supervisor

CREATE DATABASE supervisor;
\connect supervisor

CREATE TABLE config (
  id SERIAL PRIMARY KEY NOT NULL,
  "time" VARCHAR(8) NOT NULL,
  "weekdays" VARCHAR(7) NOT NULL, -- every day in the week as either yes or no
  duration INTEGER NOT NULL
);

INSERT INTO config ("time", "weekdays", duration) 
VALUES
  ('16:00:00', '1111111', 1),
  ('16:30:00', '1111111', 1),
  ('17:00:00', '1111111', 1),
  ('17:30:00', '1111111', 1),
  ('18:00:00', '1111111', 1),
  ('18:30:00', '1111111', 1),
  ('19:00:00', '1111111', 1),
  ('19:30:00', '1111111', 1),
  ('20:00:00', '1111111', 1),
  ('20:30:00', '1111111', 1),
  ('21:00:00', '1111111', 1),
  ('21:30:00', '1111111', 1),
  ('22:00:00', '1111110', 1),
  -- ('22:30:00', '1111110', 1),
  -- ('23:00:00', '1111111', 1),
  -- ('23:30:00', '1111111', 1),
  -- ('00:00:00', '1111111', 1),
  ('22:00:00', '0000001', 600);

CREATE OR REPLACE FUNCTION notify_supervisor()
  RETURNS trigger AS
  $BODY$
    BEGIN
        PERFORM pg_notify('supervisor', NEW.id::text);
        RETURN NULL;
    END; 
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER notify_supervisor
  AFTER INSERT OR UPDATE OR DELETE
  ON config
  FOR EACH ROW
  EXECUTE PROCEDURE notify_supervisor();