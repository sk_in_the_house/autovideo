import { PrismaClient } from '@prisma/client'
import { notifyDiscordServer, notifyUs } from './notifier/index'
import { thumbnail } from './interface/types'
import express from "express";
import _ from 'lodash';

const prisma = new PrismaClient()

// #1# Express setup #1#

const app = express();
app.use(express.json({ limit: '500mb' })); 
app.use(express.urlencoded({ extended: true, limit: '500mb' })); 

// #2# Express routes #2#

/**
 * Takes a blob and an url and saves it to the database
 */
app.post('/thumbnailService2/addThumbnails', async function (req, res) {

  // Receive and validate request
  const images: thumbnail[] = req.body;
  if (images == null || images == []) {
    res.status(400).send("imageArray missing");
    return;
  }

  // Save to db
  try {
    console.log("Saving thumbnails to database...");
    await prisma.thumbnail.createMany({ data: images });
    res.sendStatus(200);
  } catch (error) {
    console.log("Could not save thumbnails to database...");
    console.error(error);
    res.sendStatus(500);
  }

});


/**
 * Returns a random image and it's sourceUrl from the database
 */
app.get("/thumbnailService2/getThumbnail", async function (req, res) {

  // Get a unused thumbnail
  const thumbnail = await prisma.thumbnail.findFirst({
    where: { used: false },
  }).catch(e => {
    console.error(e);
    res.sendStatus(500);
  });

  // Check if there is at least one unused image left
  if (thumbnail) {
    // Random unused image
    res.status(200).json({ thumbnail: thumbnail });
  } else {
    // Random used image
    const anyThumbnail = await prisma.$queryRaw`SELECT * FROM thumbnail ORDER BY RANDOM() LIMIT 1`.catch(e => { console.error(e); res.status(500); });
    res.status(200).json({ thumbnail: anyThumbnail[0] ?? { id: 0, sourceurl: null, file: null, used: true }});
  }

});


/**
 * Sets the property used to true for the corresponding object in the database
 */
app.post('/thumbnailService2/setThumbnailUsed', async function (req, res) {

  // Receive and validate request
  const imageId = req.body.id;

  if (imageId == null) {
    res.status(400).send("id invalid");
    return;
  }

  // Update db

  const updateResult = await prisma.thumbnail.update({
    where: { id: imageId },
    data: { used: true },
  }).catch(e => {
    console.error(e);
    res.sendStatus(500);
  });


  // Get sourceUrl of the updated row and notify our discord server
  const image = await prisma.thumbnail.findMany({
    where: { id: imageId },
  }).catch(e => {
    console.error(e);
    res.sendStatus(500);
  });

  notifyDiscordServer(`<@&865321195456167986> Attention! We made a new video! Here's the source for it ${image[0].sourceurl}`);

  // Response
  res.status(200).json({ result: 1 });
});


/**
 * Returns a status object with various information about the service
 */
//no idea if this works
app.get('/thumbnailService2', async function (req, res) {

  const unusedImages = await prisma.thumbnail.aggregate({
    where: {
      used: false
    },
    _count: {
      sourceurl: true,
    }
  })

  const usedImages = await prisma.thumbnail.aggregate({
    where: {
      used: true
    },
    _count: {
      sourceurl: true,
    }
  })

  res.status(200).json({ unusedImages: unusedImages._count.sourceurl, usedImages: usedImages._count.sourceurl });

});

app.listen(3000);