interface thumbnail {
    id?: number; 
    sourceurl:string;
    file:string; 
    used: boolean;
}

export {
    thumbnail
}